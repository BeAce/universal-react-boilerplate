
import { fromJS } from 'immutable';
import extensionAuthPageReducer from '../reducer';

describe('extensionAuthPageReducer', () => {
  it('returns the initial state', () => {
    expect(extensionAuthPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
