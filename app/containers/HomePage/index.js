import React from 'react';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Row, Col } from 'antd';
import { userIsLogin } from 'containers/AuthWrapper';

import MainChart from 'components/HomePage/MainChart';
import TradeChart from 'components/HomePage/TradeChart';
import SaleChart from 'components/HomePage/SaleChart';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import {
  makeSelectRepos,
  makeSelectLoading,
  makeSelectError,
} from 'containers/App/selectors';
import Frame from 'containers/Frame';
import { loadRepos } from '../App/actions';
import { changeUsername } from './actions';
import { makeSelectUsername } from './selectors';
import reducer from './reducer';
import saga from './saga';
class HomePage extends React.PureComponent {
  state = {};

  render() {
    return (
      <Frame>
        <Helmet>
          <title>小程序主</title>
          <meta name="description" content="小程序主" />
        </Helmet>
        <div>
          <div style={{ marginBottom: 32 }}>
            <MainChart />
          </div>
          <Row gutter={16} type="flex">
            <Col span={12}>
              <TradeChart />
            </Col>
            <Col span={12}>
              <SaleChart />
            </Col>
          </Row>
        </div>
      </Frame>
    );
  }
}

HomePage.propTypes = {};

export function mapDispatchToProps(dispatch) {
  return {
    onChangeUsername: (evt) => dispatch(changeUsername(evt.target.value)),
    onSubmitForm: (evt) => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(loadRepos());
    },
  };
}

const mapStateToProps = createStructuredSelector({
  repos: makeSelectRepos(),
  username: makeSelectUsername(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'home', reducer });
const withSaga = injectSaga({ key: 'home', saga });

export default compose(userIsLogin, withReducer, withSaga, withConnect)(
  HomePage
);
