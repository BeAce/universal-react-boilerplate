import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import { Layout, Icon, Menu } from 'antd';
const { Header, Footer, Sider, Content } = Layout;


const LayoutStyled = styled(Layout)`
  min-height: 100vh;
  .logo {
    height: 32px;
    background: rgba(255, 255, 255, 0.2);
    margin: 16px;
    color: #fff;
    text-align: center;
    line-height: 32px;
  }
  .trigger {
    font-size: 18px;
    line-height: 64px;
    padding: 0 24px;
    cursor: pointer;
    transition: color 0.3s;
  }

  .trigger:hover {
    color: #1890ff;
  }
`;

class Frame extends React.PureComponent {
  state = {
    collapsed: false,
    current: this.props.history.location.pathname ? this.props.history.location.pathname : '/dashboard',
  };


  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  handleClick = (e) => {
    console.log('click ', e);
    this.setState({
      current: e.key,
    }, () => {
      this.props.history.push(e.key);
    });
  };

  render() {
    return (
      <LayoutStyled>
        <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
          <div className="logo">小程序主</div>
          <Menu
            theme="dark"
            mode="inline"
            onClick={this.handleClick}
            selectedKeys={[this.state.current]}
          >
            <Menu.Item key="/dashboard">
              <Icon type="user" />
              <span>统计数据</span>
            </Menu.Item>
            <Menu.Item key="channel">
              <Icon type="video-camera" />
              <span>渠道效果</span>
            </Menu.Item>
            <Menu.Item key="spread">
              <Icon type="video-camera" />
              <span>传播效果</span>
            </Menu.Item>
            <Menu.Item key="users">
              <Icon type="video-camera" />
              <span>用户留存</span>
            </Menu.Item>
            <Menu.Item key="/extends">
              <Icon type="video-camera" />
              <span>小程序推广</span>
            </Menu.Item>
            <Menu.Item key="/extends/auth">
              <Icon type="video-camera" />
              <span>推广商授权</span>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Header style={{ background: '#fff', padding: 0 }}>
            <Icon
              className="trigger"
              type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={this.toggle}
            />
          </Header>
          <Content
            style={{
              margin: '24px 16px',
              padding: 24,
              minHeight: 280,
            }}
          >
            {this.props.children}
          </Content>
          <Footer style={{ textAlign: 'center' }}>Footer</Footer>
        </Layout>
      </LayoutStyled>
    );
  }
}

Frame.propTypes = {
  history: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.array,
    PropTypes.element,
  ]),
};

export default withRouter(Frame);
