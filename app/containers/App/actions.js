import { createAction } from 'redux-actions';

import { SET_LOGIN } from './constants';

export const setLogin = createAction(SET_LOGIN);
