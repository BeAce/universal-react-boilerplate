/**
 *
 * ExtensionPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Table } from 'antd';
import styled from 'styled-components';

import Frame from 'containers/Frame';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectExtensionPage from './selectors';
import reducer from './reducer';
import saga from './saga';

const Container = styled.div`
  background-color: #fff;
  padding: 16px;
`;

const columns = [
  {
    title: '时间',
    dataIndex: 'name',
    key: 'name',
    render: (text) => <a href="">{text}</a>,
  },
  {
    title: '描述',
    dataIndex: 'age',
    key: 'age',
  },
  {
    title: '备注',
    dataIndex: 'address',
    key: 'address',
  },
  {
    title: '金额',
    dataIndex: 'address',
    key: 'address',
  },
];

const data = [
  {
    key: '1',
    name: 'John Brown',
    age: 32,
    address: 'New York No. 1 Lake Park',
  },
  {
    key: '2',
    name: 'Jim Green',
    age: 42,
    address: 'London No. 1 Lake Park',
  },
  {
    key: '3',
    name: 'Joe Black',
    age: 32,
    address: 'Sidney No. 1 Lake Park',
  },
];
export class ExtensionPage extends React.Component {
  state = {};
  render() {
    return (
      <Frame>
        <Helmet>
          <title>ExtensionPage</title>
          <meta name="description" content="Description of ExtensionPage" />
        </Helmet>
        <Container>
          <Table columns={columns} dataSource={data} />
        </Container>
      </Frame>
    );
  }
}

ExtensionPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  extensionpage: makeSelectExtensionPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'extensionPage', reducer });
const withSaga = injectSaga({ key: 'extensionPage', saga });

export default compose(withReducer, withSaga, withConnect)(ExtensionPage);
