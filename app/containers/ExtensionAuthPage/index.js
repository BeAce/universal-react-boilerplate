/**
 *
 * ExtensionAuthPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import styled from 'styled-components';
import { Form, Input, Button } from 'antd';
import Frame from 'containers/Frame';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectExtensionAuthPage from './selectors';
import reducer from './reducer';
import saga from './saga';
const FormItem = Form.Item;

const Container = styled.div`
  background-color: #fff;
  padding: 16px;
`;

class ExtensionAuthForm extends React.Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Frame>
        <Helmet>
          <title>推广商授权</title>
          <meta name="description" content="Description of ExtensionAuthPage" />
        </Helmet>
        <Container>
          <Form onSubmit={this.handleSubmit} className="login-form">
            <FormItem
              colon={false}
              label="推广商"
              labelCol={{ span: 3 }}
              wrapperCol={{ span: 12 }}
            >
              {getFieldDecorator('note', {
                rules: [{ required: true, message: '不能为空' }],
              })(<Input />)}
            </FormItem>
            <FormItem
              colon={false}
              label="申请备注"
              labelCol={{ span: 3 }}
              wrapperCol={{ span: 12 }}
            >
              {getFieldDecorator('note', {
                rules: [{ required: true, message: '不能为空' }],
              })(<Input />)}
            </FormItem>
            <FormItem
              colon={false}
              label="&nbsp;"
              labelCol={{ span: 3 }}
              wrapperCol={{ span: 12 }}
            >
              <Button type="primary" htmlType="submit">
                同意合作
              </Button>
              <Button style={{ marginLeft: 16 }}>取消</Button>
            </FormItem>
          </Form>
        </Container>
      </Frame>
    );
  }
}

ExtensionAuthForm.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  form: PropTypes.object,
};

const ExtensionAuthPage = Form.create()(ExtensionAuthForm);

const mapStateToProps = createStructuredSelector({
  extensionauthpage: makeSelectExtensionAuthPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'extensionAuthPage', reducer });
const withSaga = injectSaga({ key: 'extensionAuthPage', saga });

export default compose(withReducer, withSaga, withConnect)(ExtensionAuthPage);
