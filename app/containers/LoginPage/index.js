import React from 'react';
import { Form, Icon, Input, Button } from 'antd';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import styled from 'styled-components';
import { setLogin } from 'containers/App/actions';

const FormItem = Form.Item;

const Container = styled.div`
  width: 300px;
  margin: 0 auto;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const FormStyled = styled(Form)`
  width: 100%;
`;

const ButtonStyled = styled(Button)`
  width: 100%;
`;

class LoginPage extends React.Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        this.props.dispatch(setLogin());
        this.props.history.push('/dashboard');
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Container>
        <FormStyled onSubmit={this.handleSubmit}>
          <FormItem style={{ textAlign: 'center' }}>
            用户登录
          </FormItem>
          <FormItem>
            {getFieldDecorator('userName', {
              rules: [
                { required: true, message: 'Please input your username!' },
              ],
            })(
              <Input
                prefix={
                  <Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />
                }
                placeholder="Username"
              />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator('password', {
              rules: [
                { required: true, message: 'Please input your Password!' },
              ],
            })(
              <Input
                prefix={
                  <Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />
                }
                type="password"
                placeholder="Password"
              />
            )}
          </FormItem>
          <FormItem>
            <ButtonStyled type="primary" htmlType="submit">
              登录
            </ButtonStyled>
          </FormItem>
          <FormItem>
            <ButtonStyled href="">注册</ButtonStyled>
          </FormItem>
        </FormStyled>
      </Container>
    );
  }
}

LoginPage.propTypes = {
  form: PropTypes.object,
  dispatch: PropTypes.func,
  history: PropTypes.object,
};

const WrappedNormalLoginForm = Form.create()(LoginPage);

export function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(null, mapDispatchToProps);

// const withReducer = injectReducer({ key: 'login', reducer });
// const withSaga = injectSaga({ key: 'login', saga });

export default compose(withConnect)(WrappedNormalLoginForm);
