import { createSelector } from 'reselect';

/**
 * Direct selector to the extensionAuthPage state domain
 */
const selectExtensionAuthPageDomain = (state) => state.get('extensionAuthPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ExtensionAuthPage
 */

const makeSelectExtensionAuthPage = () => createSelector(
  selectExtensionAuthPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectExtensionAuthPage;
export {
  selectExtensionAuthPageDomain,
};
