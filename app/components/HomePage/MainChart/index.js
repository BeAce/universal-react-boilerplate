import React from 'react';
import { Tabs, Row, Col } from 'antd';
import {
  ResponsiveContainer,
  ComposedChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
} from 'recharts';
import styled from 'styled-components';
const TabPane = Tabs.TabPane;

const TabPaneStyled = styled(TabPane)`
  padding: 16px;
`;

const Num = styled.div`
  background-color: rgb(65, 62, 160);
  border-radius: 100%;
  height: 24px;
  width: 24px;
  text-align: center;
  line-height: 24px;
  color: #fff;
`;

const RowRanking = styled(Row)`
  margin-bottom: 10px;
`;

const Container = styled.div`
  background-color: #fff;
`;

class MainChart extends React.Component {
  state = {
    data: [
      { name: 'Page A', uv: 590, pv: 800, amt: 1400 },
      { name: 'Page B', uv: 868, pv: 967, amt: 1506 },
      { name: 'Page C', uv: 1397, pv: 1098, amt: 989 },
      { name: 'Page D', uv: 1480, pv: 1200, amt: 1228 },
      { name: 'Page E', uv: 1520, pv: 1108, amt: 1100 },
      { name: 'Page F', uv: 1400, pv: 680, amt: 1700 },
    ],
  };

  render() {
    return (
      <Container>
        <Tabs defaultActiveKey="1" animated={false}>
          <TabPaneStyled tab="新增" key="1">
            <Row gutter={24}>
              <Col span={16}>
                <h4>新增用户趋势</h4>
                <ResponsiveContainer width="100%" height={300}>
                  <ComposedChart
                    data={this.state.data}
                    margin={{ top: 20, right: 20, bottom: 20 }}
                  >
                    <CartesianGrid stroke="#f5f5f5" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                    <Bar dataKey="uv" barSize={20} fill="#413ea0" />
                  </ComposedChart>
                </ResponsiveContainer>
              </Col>
              <Col span={8}>
                <h4>新增排名</h4>
                <RowRanking gutter={24} style={{ marginTop: 16 }}>
                  <Col span={6}>
                    <Num>1</Num>
                  </Col>
                  <Col span={12}>xxxz小程序</Col>
                  <Col span={6}>123,123</Col>
                </RowRanking>
                <RowRanking gutter={24}>
                  <Col span={6}>
                    <Num>1</Num>
                  </Col>
                  <Col span={12}>xxxz小程序</Col>
                  <Col span={6}>123,123</Col>
                </RowRanking>
                <RowRanking gutter={24}>
                  <Col span={6}>
                    <Num>1</Num>
                  </Col>
                  <Col span={12}>xxxz小程序</Col>
                  <Col span={6}>123,123</Col>
                </RowRanking>
              </Col>
            </Row>
          </TabPaneStyled>
          <TabPaneStyled tab="活跃" key="2">
            <Row gutter={24}>
              <Col span={16}>
                <h4>新增用户趋势</h4>
                <ResponsiveContainer width="100%" height={300}>
                  <ComposedChart
                    data={this.state.data}
                    margin={{ top: 20, right: 20, bottom: 20 }}
                  >
                    <CartesianGrid stroke="#f5f5f5" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                    <Bar dataKey="uv" barSize={20} fill="#413ea0" />
                  </ComposedChart>
                </ResponsiveContainer>
              </Col>
              <Col span={8}>
                <h4>新增排名</h4>
                <RowRanking gutter={24}>
                  <Col span={6}>
                    <Num>1</Num>
                  </Col>
                  <Col span={12}>xxxz小程序</Col>
                  <Col span={6}>123,123</Col>
                </RowRanking>
                <RowRanking gutter={24}>
                  <Col span={6}>
                    <Num>1</Num>
                  </Col>
                  <Col span={12}>xxxz小程序</Col>
                  <Col span={6}>123,123</Col>
                </RowRanking>
                <RowRanking gutter={24}>
                  <Col span={6}>
                    <Num>1</Num>
                  </Col>
                  <Col span={12}>xxxz小程序</Col>
                  <Col span={6}>123,123</Col>
                </RowRanking>
              </Col>
            </Row>
          </TabPaneStyled>
        </Tabs>
      </Container>
    );
  }
}

export default MainChart;
