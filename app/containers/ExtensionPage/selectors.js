import { createSelector } from 'reselect';

/**
 * Direct selector to the extensionPage state domain
 */
const selectExtensionPageDomain = (state) => state.get('extensionPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ExtensionPage
 */

const makeSelectExtensionPage = () => createSelector(
  selectExtensionPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectExtensionPage;
export {
  selectExtensionPageDomain,
};
