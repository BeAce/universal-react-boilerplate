# universal-react-boilerplate

universal-react-boilerplate is a font-end project build by react technology stack.Her inspiration comes from [react-boileraplate](https://github.com/react-boilerplate/react-boilerplate), [expressjs-react-blog](https://github.com/BeAce/expressjs-react-blog) and [react-babel-webpack-eslint-boilerplate](https://github.com/BeAce/react-babel-webpack-eslint-boilerplate).

## Features

- [react-boilerplate#features](https://github.com/react-boilerplate/react-boilerplate#features)
- custom eslint config
- react latest(react@16.3.1)
- [redux-actions](https://github.com/redux-utilities/redux-actions)
    Help developer to get rid of `switch case` and just use hash map.
- [redux-auth-wrapper](https://github.com/mjrussell/redux-auth-wrapper)
    You can learn more about [react-redux-jwt-example](https://github.com/mjrussell/react-redux-jwt-auth-example/tree/react-router-redux)

## Removed

- i18n

## DEV

```bash
npm start
```

## PROD

```bash
npm run build
```

Start a simple http-server in `build` folder, for example:

```bash
cd build
http-server
```

## Install

```bash
npm install xxx --save(-dev)
npm build:dll
```


