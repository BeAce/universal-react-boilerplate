/**
 *
 * Asynchronously loads the component for ExtensionAuthPage
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
