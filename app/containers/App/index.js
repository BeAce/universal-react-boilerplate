/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';

import HomePage from 'containers/HomePage/Loadable';
import ExtensionPage from 'containers/ExtensionPage';
import ExtensionAuthPage from 'containers/ExtensionAuthPage';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import LoginPage from 'containers/LoginPage';
const AppWrapper = styled.div``;

export default function App() {
  return (
    <AppWrapper>
      <Helmet
        titleTemplate="%s - 统计"
        defaultTitle="统计"
      >
        <meta name="description" content="React Pro" />
      </Helmet>
      <Switch>
        <Route exact path="/login" component={LoginPage} />
        <Route exact path="/dashboard" component={HomePage} />
        <Route exact path="/extends" component={ExtensionPage} />
        <Route exact path="/extends/auth" component={ExtensionAuthPage} />
        <Route exact path="/dashboard2" component={HomePage} />
        <Route path="" component={NotFoundPage} />
      </Switch>
    </AppWrapper>
  );
}
