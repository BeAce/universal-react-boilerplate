import React from 'react';
import styled from 'styled-components';
import { Radio } from 'antd';

import { ResponsiveContainer, PieChart, Pie, Cell } from 'recharts';
const Title = styled.h4`
  border-bottom: 1px solid #f5f5f5;
  padding: 16px;
`;

const Container = styled.div`
  background-color: #fff;
  height: 100%;
`;

const Body = styled.div`
  padding: 16px;
`;

const data = [
  { name: 'Group A', value: 400 },
  { name: 'Group B', value: 300 },
  { name: 'Group C', value: 300 },
  { name: 'Group D', value: 200 },
];
const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

class TradeChart extends React.Component {
  state = {};
  render() {
    return (
      <Container>
        <Title>销售额类别占比</Title>
        <Body>
          <div style={{ marginTop: 0, marginBottom: 32 }}>
            <RadioGroup defaultValue="a">
              <RadioButton value="a">全部渠道</RadioButton>
              <RadioButton value="b">线上</RadioButton>
              <RadioButton value="c">书店</RadioButton>
            </RadioGroup>
          </div>
          <ResponsiveContainer width="100%" height={300}>
            <PieChart onMouseEnter={this.onPieEnter}>
              <Pie
                data={data}
                cx={120}
                cy={200}
                innerRadius={60}
                outerRadius={80}
                fill="#8884d8"
                paddingAngle={5}
                dataKey="name"
              >
                {data.map((entry, index) => (
                  <Cell fill={COLORS[index % COLORS.length]} key={entry.name} />
                ))}
              </Pie>
            </PieChart>
          </ResponsiveContainer>
        </Body>
      </Container>
    );
  }
}

export default TradeChart;
