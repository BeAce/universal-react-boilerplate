import { connectedRouterRedirect } from 'redux-auth-wrapper/history4/redirect';
import { makeSelectIsLogin } from 'containers/App/selectors';

export const userIsLogin = connectedRouterRedirect({
  redirectPath: '/login',
  allowRedirectBack: false,
  authenticatedSelector: (state) => {
    const isLogin = makeSelectIsLogin()(state);
    return isLogin;
  },
  wrapperDisplayName: 'UserIsLogin',
});
