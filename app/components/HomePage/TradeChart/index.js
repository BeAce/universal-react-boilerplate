import React from 'react';
import styled from 'styled-components';
import { Row, Col } from 'antd';
import {
  ResponsiveContainer,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Line,
  LineChart,
} from 'recharts';
const Container = styled.div`
  background-color: #fff;
  height: 100%;
`;

const Body = styled.div`
  padding: 16px;
`;
const Title = styled.h4`
  border-bottom: 1px solid #f5f5f5;
  padding: 16px;
`;
const Desc = styled.div`
  color: #aaa;
`;

const Number = styled.div`
  font-size: 26px;
  font-weight: bold;
`;

const data = [
  { name: 'Page A', uv: 4000 },
  { name: 'Page B', uv: 3000 },
  { name: 'Page C', uv: 2000 },
  { name: 'Page D', uv: 200 },
  { name: 'Page E', uv: 1890 },
  { name: 'Page F', uv: 2390 },
  { name: 'Page G', uv: 3490 },
];

class TradeChart extends React.Component {
  state = {};
  render() {
    return (
      <Container>
        <Title>活动实时交易情况</Title>
        <Body>
          <Row gutter={24} style={{ marginBottom: 32 }}>
            <Col span={8}>
              <Desc>今日交易总额</Desc>
              <Number>1231,123213</Number>
            </Col>
            <Col span={8}>
              <Desc>销售目标完成率</Desc>
              <Number>12%</Number>
            </Col>
            <Col span={8}>
              <Desc>每秒交易总额</Desc>
              <Number>1231,123213</Number>
            </Col>
          </Row>
          <ResponsiveContainer width="100%" height={300}>
            <LineChart
              data={data}
              margin={{ top: 10, right: 30, left: 0, bottom: 0 }}
            >
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="name" />
              <YAxis />
              <Tooltip />
              <Line
                connectNulls
                type="monotone"
                dataKey="uv"
                stroke="#8884d8"
                fill="#8884d8"
              />
            </LineChart>
          </ResponsiveContainer>
        </Body>
      </Container>
    );
  }
}

export default TradeChart;
