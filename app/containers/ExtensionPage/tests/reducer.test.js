
import { fromJS } from 'immutable';
import extensionPageReducer from '../reducer';

describe('extensionPageReducer', () => {
  it('returns the initial state', () => {
    expect(extensionPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
