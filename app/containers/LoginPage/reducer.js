import { fromJS } from 'immutable';

import { LOGIN, LOGIN_SUCCESS, LOGIN_ERROR } from './constants';

// The initial state of the App
const initialState = fromJS({
  loading: false,
});

function login(state = initialState, action) {
  switch (action.type) {
    case LOGIN:
      // Delete prefixed '@' from the github username
      return state.set('loading', true);
    case LOGIN_SUCCESS:
      return state.set('loading', false);
    case LOGIN_ERROR:
      return state.set('loading', false);
    default:
      return state;
  }
}

export default login;
